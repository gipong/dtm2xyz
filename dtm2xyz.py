import gdal, sys
from gdalconst import *
import numpy


gdal.AllRegister()

dtm = 'dtm.tif'
ds = gdal.Open(dtm, GA_ReadOnly)
if ds is None:
    print 'Cloud not open' + dtm
    sys.exit(1)

cols = ds.RasterXSize
rows = ds.RasterYSize
bands = ds.RasterCount
driver = ds.GetDriver().LongName

geotransform = ds.GetGeoTransform()

originX = geotransform[0]
originY = geotransform[3]

pixelWidth = geotransform[1]
pixelHeight = geotransform[5]

X = originX+pixelWidth/2
Y = originY+pixelHeight/2

band = ds.GetRasterBand(1)

data = band.ReadAsArray(0,0, cols, rows)

f = open('dtm2xyz.csv', 'w')
for y in range(0, rows):
    for x in range(0, cols):
        f.write(str(X+pixelWidth*x)+','+str(Y+pixelHeight*y)+','+str(data[y,x])+'\n')


f.close()
